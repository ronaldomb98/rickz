import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Provider } from "react-redux";
import AppContainer from './src/routes';
import configureStore from "./src/configureStore";
import awsmobile from './src/aws-exports';
import Amplify from "aws-amplify";

Amplify.configure(awsmobile)

let store = configureStore();
export default function App() {
  return (
    <Provider store={store}>
      <AppContainer />
    </Provider>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});

