// Index routes
import React from 'react';
import { createAppContainer, createSwitchNavigator} from 'react-navigation';
import { createStackNavigator } from "react-navigation-stack";
import LoginPage from '../pages/LoginPage';
import DashboardPage from '../pages/DashboardPage';
import QuestionListPage from '../pages/QuestionListPage';
import Question from '../pages/Question';
import exito from "../pages/exito";

const ROUTES = {
    LoginPage: 'LoginPage',
    DashboardPage: 'DashboardPage',
    QuestionListPage: 'QuestionListPage',
    QuestionPage: 'QuestionPage',
    exito: 'exito'
}

const DashboardStack = createStackNavigator({
    [ROUTES.DashboardPage] : {
        screen: DashboardPage,
        navigationOptions: {
            title: ROUTES.DashboardPage,
            header: null
        }
    },
    [ROUTES.QuestionListPage] : {
        screen: QuestionListPage,
        navigationOptions: {
            title: ROUTES.QuestionListPage,
            header: null
        }
    },
    [ROUTES.QuestionPage] : {
        screen: Question,
        navigationOptions: {
            title: ROUTES.DashboardPage,
            header: null
        }
    },
    [ROUTES.exito] : {
        screen: exito,
        navigationOptions: {
            title: ROUTES.exito,
            header: null
        }
    }
})

const LoginStack = createStackNavigator({
    [ROUTES.LoginPage]: {
        screen: LoginPage,
        navigationOptions: {
            title: ROUTES.LoginPage,
            header: null
        }
    }
});

const AppContainer = createAppContainer(
    createSwitchNavigator(
        {
            Auth: LoginStack,
            Dashboard: DashboardStack
        },
        {
            initialRouteName: 'Auth'
        }
    )
)

export default AppContainer;
export {
    ROUTES
}