const URL = "";

export const fetchSchedule = () => {
  return fetch(URL).then(Response => {
    return Promise.all([Response, Response.json()]);
  });
};
