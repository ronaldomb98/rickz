/* tslint:disable */
//  This file was automatically generated and should not be edited.

export type CreateUserInput = {
  id?: string | null,
  name: string,
  lastName: string,
  birth: string,
  adress: string,
  emergencyContact: string,
  epsId?: string | null,
};

export type UpdateUserInput = {
  id: string,
  name?: string | null,
  lastName?: string | null,
  birth?: string | null,
  adress?: string | null,
  emergencyContact?: string | null,
  epsId?: string | null,
};

export type DeleteUserInput = {
  id?: string | null,
};

export type CreateDiseaseInput = {
  id?: string | null,
  name: string,
};

export type UpdateDiseaseInput = {
  id: string,
  name?: string | null,
};

export type DeleteDiseaseInput = {
  id?: string | null,
};

export type CreateDiseaseSymptomInput = {
  id?: string | null,
  symptomId?: string | null,
  symptomFit?: number | null,
};

export type UpdateDiseaseSymptomInput = {
  id: string,
  symptomId?: string | null,
  symptomFit?: number | null,
};

export type DeleteDiseaseSymptomInput = {
  id?: string | null,
};

export type CreateSymptomInput = {
  id?: string | null,
  name: string,
};

export type UpdateSymptomInput = {
  id: string,
  name?: string | null,
};

export type DeleteSymptomInput = {
  id?: string | null,
};

export type CreateEpsInput = {
  id?: string | null,
  name: string,
  contactNumber: string,
};

export type UpdateEpsInput = {
  id: string,
  name?: string | null,
  contactNumber?: string | null,
};

export type DeleteEpsInput = {
  id?: string | null,
};

export type CreateHospitalInput = {
  id?: string | null,
  name: string,
  adress: string,
  epsId?: Array< string | null > | null,
};

export type UpdateHospitalInput = {
  id: string,
  name?: string | null,
  adress?: string | null,
  epsId?: Array< string | null > | null,
};

export type DeleteHospitalInput = {
  id?: string | null,
};

export type ModelUserFilterInput = {
  id?: ModelIDFilterInput | null,
  name?: ModelStringFilterInput | null,
  lastName?: ModelStringFilterInput | null,
  birth?: ModelStringFilterInput | null,
  adress?: ModelStringFilterInput | null,
  emergencyContact?: ModelStringFilterInput | null,
  epsId?: ModelIDFilterInput | null,
  and?: Array< ModelUserFilterInput | null > | null,
  or?: Array< ModelUserFilterInput | null > | null,
  not?: ModelUserFilterInput | null,
};

export type ModelIDFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type ModelStringFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type ModelDiseaseFilterInput = {
  id?: ModelIDFilterInput | null,
  name?: ModelStringFilterInput | null,
  and?: Array< ModelDiseaseFilterInput | null > | null,
  or?: Array< ModelDiseaseFilterInput | null > | null,
  not?: ModelDiseaseFilterInput | null,
};

export type ModelDiseaseSymptomFilterInput = {
  id?: ModelIDFilterInput | null,
  symptomId?: ModelIDFilterInput | null,
  symptomFit?: ModelIntFilterInput | null,
  and?: Array< ModelDiseaseSymptomFilterInput | null > | null,
  or?: Array< ModelDiseaseSymptomFilterInput | null > | null,
  not?: ModelDiseaseSymptomFilterInput | null,
};

export type ModelIntFilterInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  contains?: number | null,
  notContains?: number | null,
  between?: Array< number | null > | null,
};

export type ModelSymptomFilterInput = {
  id?: ModelIDFilterInput | null,
  name?: ModelStringFilterInput | null,
  and?: Array< ModelSymptomFilterInput | null > | null,
  or?: Array< ModelSymptomFilterInput | null > | null,
  not?: ModelSymptomFilterInput | null,
};

export type ModelEpsFilterInput = {
  id?: ModelIDFilterInput | null,
  name?: ModelStringFilterInput | null,
  contactNumber?: ModelStringFilterInput | null,
  and?: Array< ModelEpsFilterInput | null > | null,
  or?: Array< ModelEpsFilterInput | null > | null,
  not?: ModelEpsFilterInput | null,
};

export type ModelHospitalFilterInput = {
  id?: ModelIDFilterInput | null,
  name?: ModelStringFilterInput | null,
  adress?: ModelStringFilterInput | null,
  epsId?: ModelIDFilterInput | null,
  and?: Array< ModelHospitalFilterInput | null > | null,
  or?: Array< ModelHospitalFilterInput | null > | null,
  not?: ModelHospitalFilterInput | null,
};

export type CreateUserMutationVariables = {
  input: CreateUserInput,
};

export type CreateUserMutation = {
  createUser:  {
    __typename: "User",
    id: string,
    name: string,
    lastName: string,
    birth: string,
    adress: string,
    emergencyContact: string,
    epsId: string | null,
  } | null,
};

export type UpdateUserMutationVariables = {
  input: UpdateUserInput,
};

export type UpdateUserMutation = {
  updateUser:  {
    __typename: "User",
    id: string,
    name: string,
    lastName: string,
    birth: string,
    adress: string,
    emergencyContact: string,
    epsId: string | null,
  } | null,
};

export type DeleteUserMutationVariables = {
  input: DeleteUserInput,
};

export type DeleteUserMutation = {
  deleteUser:  {
    __typename: "User",
    id: string,
    name: string,
    lastName: string,
    birth: string,
    adress: string,
    emergencyContact: string,
    epsId: string | null,
  } | null,
};

export type CreateDiseaseMutationVariables = {
  input: CreateDiseaseInput,
};

export type CreateDiseaseMutation = {
  createDisease:  {
    __typename: "Disease",
    id: string,
    name: string,
    symptoms:  Array< {
      __typename: "DiseaseSymptom",
      id: string | null,
      symptomId: string | null,
      symptomFit: number | null,
    } | null > | null,
  } | null,
};

export type UpdateDiseaseMutationVariables = {
  input: UpdateDiseaseInput,
};

export type UpdateDiseaseMutation = {
  updateDisease:  {
    __typename: "Disease",
    id: string,
    name: string,
    symptoms:  Array< {
      __typename: "DiseaseSymptom",
      id: string | null,
      symptomId: string | null,
      symptomFit: number | null,
    } | null > | null,
  } | null,
};

export type DeleteDiseaseMutationVariables = {
  input: DeleteDiseaseInput,
};

export type DeleteDiseaseMutation = {
  deleteDisease:  {
    __typename: "Disease",
    id: string,
    name: string,
    symptoms:  Array< {
      __typename: "DiseaseSymptom",
      id: string | null,
      symptomId: string | null,
      symptomFit: number | null,
    } | null > | null,
  } | null,
};

export type CreateDiseaseSymptomMutationVariables = {
  input: CreateDiseaseSymptomInput,
};

export type CreateDiseaseSymptomMutation = {
  createDiseaseSymptom:  {
    __typename: "DiseaseSymptom",
    id: string | null,
    symptomId: string | null,
    symptomFit: number | null,
  } | null,
};

export type UpdateDiseaseSymptomMutationVariables = {
  input: UpdateDiseaseSymptomInput,
};

export type UpdateDiseaseSymptomMutation = {
  updateDiseaseSymptom:  {
    __typename: "DiseaseSymptom",
    id: string | null,
    symptomId: string | null,
    symptomFit: number | null,
  } | null,
};

export type DeleteDiseaseSymptomMutationVariables = {
  input: DeleteDiseaseSymptomInput,
};

export type DeleteDiseaseSymptomMutation = {
  deleteDiseaseSymptom:  {
    __typename: "DiseaseSymptom",
    id: string | null,
    symptomId: string | null,
    symptomFit: number | null,
  } | null,
};

export type CreateSymptomMutationVariables = {
  input: CreateSymptomInput,
};

export type CreateSymptomMutation = {
  createSymptom:  {
    __typename: "Symptom",
    id: string,
    name: string,
  } | null,
};

export type UpdateSymptomMutationVariables = {
  input: UpdateSymptomInput,
};

export type UpdateSymptomMutation = {
  updateSymptom:  {
    __typename: "Symptom",
    id: string,
    name: string,
  } | null,
};

export type DeleteSymptomMutationVariables = {
  input: DeleteSymptomInput,
};

export type DeleteSymptomMutation = {
  deleteSymptom:  {
    __typename: "Symptom",
    id: string,
    name: string,
  } | null,
};

export type CreateEpsMutationVariables = {
  input: CreateEpsInput,
};

export type CreateEpsMutation = {
  createEps:  {
    __typename: "Eps",
    id: string,
    name: string,
    contactNumber: string,
  } | null,
};

export type UpdateEpsMutationVariables = {
  input: UpdateEpsInput,
};

export type UpdateEpsMutation = {
  updateEps:  {
    __typename: "Eps",
    id: string,
    name: string,
    contactNumber: string,
  } | null,
};

export type DeleteEpsMutationVariables = {
  input: DeleteEpsInput,
};

export type DeleteEpsMutation = {
  deleteEps:  {
    __typename: "Eps",
    id: string,
    name: string,
    contactNumber: string,
  } | null,
};

export type CreateHospitalMutationVariables = {
  input: CreateHospitalInput,
};

export type CreateHospitalMutation = {
  createHospital:  {
    __typename: "Hospital",
    id: string,
    name: string,
    adress: string,
    epsId: Array< string | null > | null,
  } | null,
};

export type UpdateHospitalMutationVariables = {
  input: UpdateHospitalInput,
};

export type UpdateHospitalMutation = {
  updateHospital:  {
    __typename: "Hospital",
    id: string,
    name: string,
    adress: string,
    epsId: Array< string | null > | null,
  } | null,
};

export type DeleteHospitalMutationVariables = {
  input: DeleteHospitalInput,
};

export type DeleteHospitalMutation = {
  deleteHospital:  {
    __typename: "Hospital",
    id: string,
    name: string,
    adress: string,
    epsId: Array< string | null > | null,
  } | null,
};

export type GetUserQueryVariables = {
  id: string,
};

export type GetUserQuery = {
  getUser:  {
    __typename: "User",
    id: string,
    name: string,
    lastName: string,
    birth: string,
    adress: string,
    emergencyContact: string,
    epsId: string | null,
  } | null,
};

export type ListUsersQueryVariables = {
  filter?: ModelUserFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListUsersQuery = {
  listUsers:  {
    __typename: "ModelUserConnection",
    items:  Array< {
      __typename: "User",
      id: string,
      name: string,
      lastName: string,
      birth: string,
      adress: string,
      emergencyContact: string,
      epsId: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetDiseaseQueryVariables = {
  id: string,
};

export type GetDiseaseQuery = {
  getDisease:  {
    __typename: "Disease",
    id: string,
    name: string,
    symptoms:  Array< {
      __typename: "DiseaseSymptom",
      id: string | null,
      symptomId: string | null,
      symptomFit: number | null,
    } | null > | null,
  } | null,
};

export type ListDiseasesQueryVariables = {
  filter?: ModelDiseaseFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListDiseasesQuery = {
  listDiseases:  {
    __typename: "ModelDiseaseConnection",
    items:  Array< {
      __typename: "Disease",
      id: string,
      name: string,
      symptoms:  Array< {
        __typename: "DiseaseSymptom",
        id: string | null,
        symptomId: string | null,
        symptomFit: number | null,
      } | null > | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetDiseaseSymptomQueryVariables = {
  id: string,
};

export type GetDiseaseSymptomQuery = {
  getDiseaseSymptom:  {
    __typename: "DiseaseSymptom",
    id: string | null,
    symptomId: string | null,
    symptomFit: number | null,
  } | null,
};

export type ListDiseaseSymptomsQueryVariables = {
  filter?: ModelDiseaseSymptomFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListDiseaseSymptomsQuery = {
  listDiseaseSymptoms:  {
    __typename: "ModelDiseaseSymptomConnection",
    items:  Array< {
      __typename: "DiseaseSymptom",
      id: string | null,
      symptomId: string | null,
      symptomFit: number | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetSymptomQueryVariables = {
  id: string,
};

export type GetSymptomQuery = {
  getSymptom:  {
    __typename: "Symptom",
    id: string,
    name: string,
  } | null,
};

export type ListSymptomsQueryVariables = {
  filter?: ModelSymptomFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListSymptomsQuery = {
  listSymptoms:  {
    __typename: "ModelSymptomConnection",
    items:  Array< {
      __typename: "Symptom",
      id: string,
      name: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetEpsQueryVariables = {
  id: string,
};

export type GetEpsQuery = {
  getEps:  {
    __typename: "Eps",
    id: string,
    name: string,
    contactNumber: string,
  } | null,
};

export type ListEpssQueryVariables = {
  filter?: ModelEpsFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListEpssQuery = {
  listEpss:  {
    __typename: "ModelEpsConnection",
    items:  Array< {
      __typename: "Eps",
      id: string,
      name: string,
      contactNumber: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetHospitalQueryVariables = {
  id: string,
};

export type GetHospitalQuery = {
  getHospital:  {
    __typename: "Hospital",
    id: string,
    name: string,
    adress: string,
    epsId: Array< string | null > | null,
  } | null,
};

export type ListHospitalsQueryVariables = {
  filter?: ModelHospitalFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListHospitalsQuery = {
  listHospitals:  {
    __typename: "ModelHospitalConnection",
    items:  Array< {
      __typename: "Hospital",
      id: string,
      name: string,
      adress: string,
      epsId: Array< string | null > | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type OnCreateUserSubscription = {
  onCreateUser:  {
    __typename: "User",
    id: string,
    name: string,
    lastName: string,
    birth: string,
    adress: string,
    emergencyContact: string,
    epsId: string | null,
  } | null,
};

export type OnUpdateUserSubscription = {
  onUpdateUser:  {
    __typename: "User",
    id: string,
    name: string,
    lastName: string,
    birth: string,
    adress: string,
    emergencyContact: string,
    epsId: string | null,
  } | null,
};

export type OnDeleteUserSubscription = {
  onDeleteUser:  {
    __typename: "User",
    id: string,
    name: string,
    lastName: string,
    birth: string,
    adress: string,
    emergencyContact: string,
    epsId: string | null,
  } | null,
};

export type OnCreateDiseaseSubscription = {
  onCreateDisease:  {
    __typename: "Disease",
    id: string,
    name: string,
    symptoms:  Array< {
      __typename: "DiseaseSymptom",
      id: string | null,
      symptomId: string | null,
      symptomFit: number | null,
    } | null > | null,
  } | null,
};

export type OnUpdateDiseaseSubscription = {
  onUpdateDisease:  {
    __typename: "Disease",
    id: string,
    name: string,
    symptoms:  Array< {
      __typename: "DiseaseSymptom",
      id: string | null,
      symptomId: string | null,
      symptomFit: number | null,
    } | null > | null,
  } | null,
};

export type OnDeleteDiseaseSubscription = {
  onDeleteDisease:  {
    __typename: "Disease",
    id: string,
    name: string,
    symptoms:  Array< {
      __typename: "DiseaseSymptom",
      id: string | null,
      symptomId: string | null,
      symptomFit: number | null,
    } | null > | null,
  } | null,
};

export type OnCreateDiseaseSymptomSubscription = {
  onCreateDiseaseSymptom:  {
    __typename: "DiseaseSymptom",
    id: string | null,
    symptomId: string | null,
    symptomFit: number | null,
  } | null,
};

export type OnUpdateDiseaseSymptomSubscription = {
  onUpdateDiseaseSymptom:  {
    __typename: "DiseaseSymptom",
    id: string | null,
    symptomId: string | null,
    symptomFit: number | null,
  } | null,
};

export type OnDeleteDiseaseSymptomSubscription = {
  onDeleteDiseaseSymptom:  {
    __typename: "DiseaseSymptom",
    id: string | null,
    symptomId: string | null,
    symptomFit: number | null,
  } | null,
};

export type OnCreateSymptomSubscription = {
  onCreateSymptom:  {
    __typename: "Symptom",
    id: string,
    name: string,
  } | null,
};

export type OnUpdateSymptomSubscription = {
  onUpdateSymptom:  {
    __typename: "Symptom",
    id: string,
    name: string,
  } | null,
};

export type OnDeleteSymptomSubscription = {
  onDeleteSymptom:  {
    __typename: "Symptom",
    id: string,
    name: string,
  } | null,
};

export type OnCreateEpsSubscription = {
  onCreateEps:  {
    __typename: "Eps",
    id: string,
    name: string,
    contactNumber: string,
  } | null,
};

export type OnUpdateEpsSubscription = {
  onUpdateEps:  {
    __typename: "Eps",
    id: string,
    name: string,
    contactNumber: string,
  } | null,
};

export type OnDeleteEpsSubscription = {
  onDeleteEps:  {
    __typename: "Eps",
    id: string,
    name: string,
    contactNumber: string,
  } | null,
};

export type OnCreateHospitalSubscription = {
  onCreateHospital:  {
    __typename: "Hospital",
    id: string,
    name: string,
    adress: string,
    epsId: Array< string | null > | null,
  } | null,
};

export type OnUpdateHospitalSubscription = {
  onUpdateHospital:  {
    __typename: "Hospital",
    id: string,
    name: string,
    adress: string,
    epsId: Array< string | null > | null,
  } | null,
};

export type OnDeleteHospitalSubscription = {
  onDeleteHospital:  {
    __typename: "Hospital",
    id: string,
    name: string,
    adress: string,
    epsId: Array< string | null > | null,
  } | null,
};
