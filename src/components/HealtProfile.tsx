import React, { Component } from "react";
import { View, Text, ScrollView } from "react-native";
import { connect } from "react-redux";
import { fetchData } from "../actions";

class ShowListComponent extends Component {
  
  componentWillMount() {
    this.props.fetchData();
  }

  render() {
    const { data } = this.props;
    return (
      <View>
        <Text>Perfil</Text>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.data
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchData: () => {
      return dispatch(fetchData());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowListComponent);
