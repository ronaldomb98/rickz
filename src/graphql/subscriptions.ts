// tslint:disable
// this is an auto generated file. This will be overwritten

export const onCreateUser = `subscription OnCreateUser {
  onCreateUser {
    id
    name
    lastName
    birth
    adress
    emergencyContact
    epsId
  }
}
`;
export const onUpdateUser = `subscription OnUpdateUser {
  onUpdateUser {
    id
    name
    lastName
    birth
    adress
    emergencyContact
    epsId
  }
}
`;
export const onDeleteUser = `subscription OnDeleteUser {
  onDeleteUser {
    id
    name
    lastName
    birth
    adress
    emergencyContact
    epsId
  }
}
`;
export const onCreateDisease = `subscription OnCreateDisease {
  onCreateDisease {
    id
    name
    symptoms {
      id
      symptomId
      symptomFit
    }
  }
}
`;
export const onUpdateDisease = `subscription OnUpdateDisease {
  onUpdateDisease {
    id
    name
    symptoms {
      id
      symptomId
      symptomFit
    }
  }
}
`;
export const onDeleteDisease = `subscription OnDeleteDisease {
  onDeleteDisease {
    id
    name
    symptoms {
      id
      symptomId
      symptomFit
    }
  }
}
`;
export const onCreateDiseaseSymptom = `subscription OnCreateDiseaseSymptom {
  onCreateDiseaseSymptom {
    id
    symptomId
    symptomFit
  }
}
`;
export const onUpdateDiseaseSymptom = `subscription OnUpdateDiseaseSymptom {
  onUpdateDiseaseSymptom {
    id
    symptomId
    symptomFit
  }
}
`;
export const onDeleteDiseaseSymptom = `subscription OnDeleteDiseaseSymptom {
  onDeleteDiseaseSymptom {
    id
    symptomId
    symptomFit
  }
}
`;
export const onCreateSymptom = `subscription OnCreateSymptom {
  onCreateSymptom {
    id
    name
  }
}
`;
export const onUpdateSymptom = `subscription OnUpdateSymptom {
  onUpdateSymptom {
    id
    name
  }
}
`;
export const onDeleteSymptom = `subscription OnDeleteSymptom {
  onDeleteSymptom {
    id
    name
  }
}
`;
export const onCreateEps = `subscription OnCreateEps {
  onCreateEps {
    id
    name
    contactNumber
  }
}
`;
export const onUpdateEps = `subscription OnUpdateEps {
  onUpdateEps {
    id
    name
    contactNumber
  }
}
`;
export const onDeleteEps = `subscription OnDeleteEps {
  onDeleteEps {
    id
    name
    contactNumber
  }
}
`;
export const onCreateHospital = `subscription OnCreateHospital {
  onCreateHospital {
    id
    name
    adress
    epsId
  }
}
`;
export const onUpdateHospital = `subscription OnUpdateHospital {
  onUpdateHospital {
    id
    name
    adress
    epsId
  }
}
`;
export const onDeleteHospital = `subscription OnDeleteHospital {
  onDeleteHospital {
    id
    name
    adress
    epsId
  }
}
`;
