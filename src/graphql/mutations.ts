// tslint:disable
// this is an auto generated file. This will be overwritten

export const createUser = `mutation CreateUser($input: CreateUserInput!) {
  createUser(input: $input) {
    id
    name
    lastName
    birth
    adress
    emergencyContact
    epsId
  }
}
`;
export const updateUser = `mutation UpdateUser($input: UpdateUserInput!) {
  updateUser(input: $input) {
    id
    name
    lastName
    birth
    adress
    emergencyContact
    epsId
  }
}
`;
export const deleteUser = `mutation DeleteUser($input: DeleteUserInput!) {
  deleteUser(input: $input) {
    id
    name
    lastName
    birth
    adress
    emergencyContact
    epsId
  }
}
`;
export const createDisease = `mutation CreateDisease($input: CreateDiseaseInput!) {
  createDisease(input: $input) {
    id
    name
    symptoms {
      id
      symptomId
      symptomFit
    }
  }
}
`;
export const updateDisease = `mutation UpdateDisease($input: UpdateDiseaseInput!) {
  updateDisease(input: $input) {
    id
    name
    symptoms {
      id
      symptomId
      symptomFit
    }
  }
}
`;
export const deleteDisease = `mutation DeleteDisease($input: DeleteDiseaseInput!) {
  deleteDisease(input: $input) {
    id
    name
    symptoms {
      id
      symptomId
      symptomFit
    }
  }
}
`;
export const createDiseaseSymptom = `mutation CreateDiseaseSymptom($input: CreateDiseaseSymptomInput!) {
  createDiseaseSymptom(input: $input) {
    id
    symptomId
    symptomFit
  }
}
`;
export const updateDiseaseSymptom = `mutation UpdateDiseaseSymptom($input: UpdateDiseaseSymptomInput!) {
  updateDiseaseSymptom(input: $input) {
    id
    symptomId
    symptomFit
  }
}
`;
export const deleteDiseaseSymptom = `mutation DeleteDiseaseSymptom($input: DeleteDiseaseSymptomInput!) {
  deleteDiseaseSymptom(input: $input) {
    id
    symptomId
    symptomFit
  }
}
`;
export const createSymptom = `mutation CreateSymptom($input: CreateSymptomInput!) {
  createSymptom(input: $input) {
    id
    name
  }
}
`;
export const updateSymptom = `mutation UpdateSymptom($input: UpdateSymptomInput!) {
  updateSymptom(input: $input) {
    id
    name
  }
}
`;
export const deleteSymptom = `mutation DeleteSymptom($input: DeleteSymptomInput!) {
  deleteSymptom(input: $input) {
    id
    name
  }
}
`;
export const createEps = `mutation CreateEps($input: CreateEpsInput!) {
  createEps(input: $input) {
    id
    name
    contactNumber
  }
}
`;
export const updateEps = `mutation UpdateEps($input: UpdateEpsInput!) {
  updateEps(input: $input) {
    id
    name
    contactNumber
  }
}
`;
export const deleteEps = `mutation DeleteEps($input: DeleteEpsInput!) {
  deleteEps(input: $input) {
    id
    name
    contactNumber
  }
}
`;
export const createHospital = `mutation CreateHospital($input: CreateHospitalInput!) {
  createHospital(input: $input) {
    id
    name
    adress
    epsId
  }
}
`;
export const updateHospital = `mutation UpdateHospital($input: UpdateHospitalInput!) {
  updateHospital(input: $input) {
    id
    name
    adress
    epsId
  }
}
`;
export const deleteHospital = `mutation DeleteHospital($input: DeleteHospitalInput!) {
  deleteHospital(input: $input) {
    id
    name
    adress
    epsId
  }
}
`;
