// tslint:disable
// this is an auto generated file. This will be overwritten

export const getUser = `query GetUser($id: ID!) {
  getUser(id: $id) {
    id
    name
    lastName
    birth
    adress
    emergencyContact
    epsId
  }
}
`;
export const listUsers = `query ListUsers(
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      name
      lastName
      birth
      adress
      emergencyContact
      epsId
    }
    nextToken
  }
}
`;
export const getDisease = `query GetDisease($id: ID!) {
  getDisease(id: $id) {
    id
    name
    symptoms {
      id
      symptomId
      symptomFit
    }
  }
}
`;
export const listDiseases = `query ListDiseases(
  $filter: ModelDiseaseFilterInput
  $limit: Int
  $nextToken: String
) {
  listDiseases(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      name
      symptoms {
        id
        symptomId
        symptomFit
      }
    }
    nextToken
  }
}
`;
export const getDiseaseSymptom = `query GetDiseaseSymptom($id: ID!) {
  getDiseaseSymptom(id: $id) {
    id
    symptomId
    symptomFit
  }
}
`;
export const listDiseaseSymptoms = `query ListDiseaseSymptoms(
  $filter: ModelDiseaseSymptomFilterInput
  $limit: Int
  $nextToken: String
) {
  listDiseaseSymptoms(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      symptomId
      symptomFit
    }
    nextToken
  }
}
`;
export const getSymptom = `query GetSymptom($id: ID!) {
  getSymptom(id: $id) {
    id
    name
  }
}
`;
export const listSymptoms = `query ListSymptoms(
  $filter: ModelSymptomFilterInput
  $limit: Int
  $nextToken: String
) {
  listSymptoms(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      name
    }
    nextToken
  }
}
`;
export const getEps = `query GetEps($id: ID!) {
  getEps(id: $id) {
    id
    name
    contactNumber
  }
}
`;
export const listEpss = `query ListEpss($filter: ModelEpsFilterInput, $limit: Int, $nextToken: String) {
  listEpss(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      name
      contactNumber
    }
    nextToken
  }
}
`;
export const getHospital = `query GetHospital($id: ID!) {
  getHospital(id: $id) {
    id
    name
    adress
    epsId
  }
}
`;
export const listHospitals = `query ListHospitals(
  $filter: ModelHospitalFilterInput
  $limit: Int
  $nextToken: String
) {
  listHospitals(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      name
      adress
      epsId
    }
    nextToken
  }
}
`;
