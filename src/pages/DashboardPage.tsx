import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { Text, Overlay, Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import { ROUTES } from "../routes";

export default class DashboardPage extends Component<any, any> {
  constructor(props) {
    super(props);
    this.showAlert = this.showAlert.bind(this);
    this.goToTraces = this.goToTraces.bind(this);
    this.state = {
      showNotification: false
    };
  }
  goToTraces() {
    this.props.navigation.navigate(ROUTES.QuestionPage);
  }

  showAlert(state) {
    this.setState({ showNotification: state });
  }

  render() {
    return (
      <View style={styles.content}>
        {this.state.showNotification && (
          <Overlay isVisible={this.state.isVisible} width="auto" height="auto">
            <View style={styles.content}>
              <Text style={styles.help}>La ayuda va en camino!!</Text>
              <Icon name="ambulance" size={90} color="black" />
              <Button
                title=" Ok "
                buttonStyle={{  margin: 100 }}
                onPress={() => this.showAlert(false)}
              />
            </View>
          </Overlay>
        )}
        <Text style={styles.question}>Qué te pasó?</Text>
        <Button
          title="Sufrí un accidente"
          buttonStyle={{ ...styles.button, backgroundColor: "#3c70ec" }}
          onPress={() => this.showAlert(true)}
        />
        <Button
          title="Tengo malestar"
          buttonStyle={{ ...styles.button, backgroundColor: "#7512bd" }}
          onPress={this.goToTraces}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    width: "100%",
    paddingVertical: 100,
    marginHorizontal: 5,
    marginVertical: 30,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  content: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  question: {
    margin: 10,
    fontSize: 40,
    color: "#3e6deb"
  },
  help: {
    display: "flex",
    margin: 10,
    fontSize: 60,
    color: "#3e6deb",
    justifyContent: "center"
  }
});
