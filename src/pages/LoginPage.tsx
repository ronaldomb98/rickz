// Initial Login page

import React, { Component } from 'react';
import { Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import { Input, Button } from 'react-native-elements';
import { Auth } from 'aws-amplify';
import { ROUTES } from '../routes';

type LoginPageState = {phoneNumber?: string, password?: string, isLoading?: boolean};

export default class LoginPage extends Component<any, LoginPageState> {

  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.updateInput = this.updateInput.bind(this);
    this.state = {
      phoneNumber: null,
      password: null,
      isLoading: false
    }
  }

  updateInput = (name: any, value: any) => this.setState({ [name]: value });

  login() {
    //const { phoneNumber, password } = this.state;
    this.setState({isLoading: true});
    this.props.navigation.navigate(ROUTES.DashboardPage);
    /* Auth.signIn(phoneNumber, password)
      .then(res => {
        this.setState({ isLoading: false });
      }) */
  }

  render() {
    return (
      <View>
          <Input
              placeholder='Número de telefono'
              onChangeText={value => this.updateInput('phoneNumber', value)}
              value={this.state.phoneNumber}
              leftIcon={
                <Icon
                  name='user'
                  size={24}
                  color='black'
                />
              }
          />
          <Input
              placeholder='Contraseña'
              onChangeText={value => this.updateInput('password', value)}
              keyboardType='default'
              textContentType='password'
              secureTextEntry={true}
              value={this.state.password}
              leftIcon={
                <Icon
                  name='key'
                  size={24}
                  color='black'
                />
              }
          />
          <Button
            title='INGRESAR'
            onPress={this.login}
          />
      </View>
      );
  }
}