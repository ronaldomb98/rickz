import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import { Input, Button } from 'react-native-elements';
import { Auth } from 'aws-amplify';
import { ROUTES } from '../routes';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


class exitComponent extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.pageContainer}>
                <View style={styles.circulo}>
                    <Text>¡Listo!</Text>
                </View>
                <Text style={styles.subtitle}>Tu cita ya esta asignada</Text>
                <Text style={styles.subtitle}>No olvidesestar 20 minutos antes</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    pageContainer: {
        padding: hp('%5'),
        display: "flex",
    },

    circulo: {
        alignSelf: "center",
        borderRadius: 100 / 2,
        backgroundColor: "#3875EE",
        color: "#fff",
    },

    subtitle: {
        marginTop: hp('%8'),
        color: "#3875EE",
    }


})
export default exitComponent;