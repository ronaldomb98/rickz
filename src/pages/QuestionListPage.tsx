import React, { Component } from "react";
import { View, ScrollView, FlatList, StyleSheet } from "react-native";
import { ListItem, Slider, Text, Button } from 'react-native-elements'
import { ROUTES } from "../routes";
import { heightPercentageToDP as hp, widthPercentageToDP as wh } from "react-native-responsive-screen";
const list = [
    { id: 1, name: "Agotamiento", value: 0 },
    { id: 2, name: "Ardor al orinar", value: 0 },
    { id: 3, name: "ardor en los ojos", value: 0 },
    { id: 4, name: "ardor gastrico", value: 0 },
    { id: 5, name: "Decoloración de las uñas", value: 0 },
    { id: 6, name: "Diarrea", value: 0 },
    { id: 7, name: "Dificultad respiratoria", value: 0 },
    { id: 8, name: "Dolor abdominal", value: 0 },
    { id: 9, name: "Dolor articulaciones", value: 0 },
    { id: 10, name: "Dolor de cabeza", value: 0 },
    { id: 11, name: "dolor de garganta", value: 0 },
    { id: 12, name: "eruptos", value: 0 },
    { id: 13, name: "estornudo", value: 0 },
    { id: 14, name: "Estreñimiento", value: 0 },
    { id: 15, name: "fatiga", value: 0 },
    { id: 16, name: "Fiebre", value: 0 },
    { id: 17, name: "hinchazón", value: 0 },
    { id: 18, name: "moqueo nasal", value: 0 },
    { id: 19, name: "ojos llorosos", value: 0 },
    { id: 20, name: "Picor", value: 0 },
    { id: 21, name: "reflujo", value: 0 },
    { id: 22, name: "Sangre en las heces", value: 0 },
    { id: 23, name:"Tos", value: 0 },
]
export default class QuestionListPage extends Component<any, any> {

    constructor(props) {
        super(props);
        this.state = {
            symptoms: list,
            question: 'Mide qué sientes'
        }
    }

    render() {
        const symptoms = [...this.state.symptoms]
        const question = this.state.question;
        return (<View>
            <Text>{question}</Text>
            <View style={styles.container}>
                <FlatList  keyExtractor={this.keyExtractor} data={symptoms} renderItem={this.renderItemList} />
            </View>
            <Button
                title='CONTINUAR'
                onPress={this.continue}
          />
        </View>)
    }

    continue = () => this.props.navigation.navigate(ROUTES.QuestionListPage);

    keyExtractor = (item, index) => index.toString()

    renderItemList = ({item: {id, name, value}}) => (<ListItem title={name} rightElement={this.renderRightElement({id, value})}/>)

    renderRightElement = ({id, value}) => <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
        <Slider value={value} maximumValue={5} step={1} onValueChange={value => this.onSlideChange({ id, value })} />
    </View>

    onSlideChange = ({id, value}) => {
        const symptoms = [...this.state.symptoms];
        const itemToModify = symptoms.findIndex(el => el.id === id);
        symptoms[itemToModify].value = value;
        this.setState(symptoms);
    }
}

const styles = StyleSheet.create({
    container: {
      maxHeight: hp('80%')
    }
  });