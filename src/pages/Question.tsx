import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';
import { ButtonGroup } from 'react-native-elements';
import { connect } from 'react-redux';
import { fetchData } from '../actions';
import { LinearGradient } from 'expo-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class ShowListComponent extends Component {
  constructor(props) {
    super(props);
    this.selectOption = this.selectOption.bind(this);
  }

  selectOption(option) {
      console.log(option)
    if(option === 0) {
        console.log('working')
    } else {
        console.log('not workiking')
    }
  }

  render() {
    const buttons = ['SI', 'NO'];
    return (
      <LinearGradient style={styles.mainContainer} colors={['#3875EE', '#7909B7']}>
        <Text style={styles.title}>¿Que estas sitiendo?</Text>
        <View style={styles.questionStyleComponent}>
          <Text style={styles.questionStyle}>¿Te encuentras actualmente embarazada?</Text>
          <ButtonGroup
            onPress={this.selectOption}
            buttons={buttons}
            containerStyle={{ height: 100 }}
          />
        </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
    mainContainer: {
        padding: hp('4%'),
        height: hp('100%')
    },
    title: {
        marginTop: hp('10%'),
        textAlign: 'center',
        color: 'white',
        fontSize: hp('5%'),
    },
    questionStyle: {
        textAlign: 'center',
        color: '#3875EE',
        fontSize: hp('4%'),
        marginBottom: hp('12%')
    },
    questionStyleComponent: {
        backgroundColor: 'white',
        borderRadius: hp('3%'),
        padding: hp('3%'),
        height: hp('60%'),
        marginTop: hp('15%')
    }
})

const mapStateToProps = state => {
  return {
    data: state.data
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchData: () => {
      return dispatch(fetchData());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowListComponent);
