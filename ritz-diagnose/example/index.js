'use strict'

const diagnoser = require('../')

console.log(diagnoser([
    {
        id: 1,
        levelOfPain: 4
    }, {
        id: 2,
        levelOfPain: 5
    }
]))