'use strict'

const diseases = require('./data/disease.json')
const urgencyLevels = require('./data/urgencyLevels')
const hospitals = require('./data/hospital.json')

module.exports = {
    diagnose,
    diseases,
    urgencyLevels,
    hospitals
}

function diagnose (symptoms) {
    if(typeof(symptoms) !== 'object')
        throw new Error('you must to provide an array')
    let currentDiagnose = { code: 0, name: "Consulta externa", fit: 0 }

    for(const disease of diseases) {
        const fit = evalDiseaseFit(disease, symptoms)
        if(currentDiagnose.fit < fit) {
            console.log(fit, evalUrgencyLevel(fit))
            currentDiagnose = Object.assign(evalUrgencyLevel(fit), { fit, disease })
        }
    }
    return currentDiagnose
}

function evalDiseaseFit (disease, symptoms) {
    let result = 0
    for(const symptom of symptoms) {
        for(const diseaseSymptom of disease.symptoms) {
            result += (diseaseSymptom == symptom.id) ? symptom.levelOfPain : 0
        }
    }
    console.log(result)
    return result
}

function evalUrgencyLevel (diseaseFit) {
    let result = urgencyLevels[0]
    for(const urgencyLevel of urgencyLevels)
        if( diseaseFit >= urgencyLevel.requiredFit) result = urgencyLevel
    return result
}